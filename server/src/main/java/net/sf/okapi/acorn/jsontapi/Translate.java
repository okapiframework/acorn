/*===========================================================================
  Copyright (C) 2015 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.acorn.jsontapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

@Path("translate")
public class Translate {
	
	private @Context ServletContext context;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response translation_GET (@QueryParam("src") String source,
		@QueryParam("trg") String target,
		@QueryParam("text") String text)
	{
		System.out.println("=== GET request:");
		System.out.println("src="+source+", trg="+target+", text="+text);
		return translate(source, target, text);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response translation_POST (@QueryParam("src") String source,
		@QueryParam("trg") String target,
		InputStream inputStream)
	{
		System.out.println("=== POST request:");
		System.out.println("src="+source+", trg="+target+", input="+inputStream);
		StringBuilder input = new StringBuilder();
		
		try ( BufferedReader br = new BufferedReader(new InputStreamReader(
			inputStream, StandardCharsets.UTF_8)) )
		{
			String line = null;
			while (( line = br.readLine() ) != null ) {
				input.append(line);
			}
		}
		catch ( IOException e ) {
			e.printStackTrace();
			return ErrorResponse.create(Response.Status.BAD_REQUEST,
				"IO error:\n"+e.getMessage());
		}
		System.out.println("payload: "+input.toString());
		return translate(source, target, input.toString());
	}

	private Response translate (String source,
		String target,
		String jsonString)
	{
		if (( source == null ) || source.isEmpty() ) {
			return ErrorResponse.create(Response.Status.BAD_REQUEST,
				"Invalid source language '"+source+"'");
		}
		if (( target == null ) || target.isEmpty() ) {
			return ErrorResponse.create(Response.Status.BAD_REQUEST,
				"Invalid target language '"+target+"'");
		}
		if (( jsonString == null ) || jsonString.isEmpty() ) {
			return ErrorResponse.create(Response.Status.BAD_REQUEST,
				"Invalid JSON content '"+jsonString+"'");
		}
		
		String res = DataStore.SI.processJSON(jsonString, source, target);
		
		ResponseBuilder rb = Response.ok(res, MediaType.APPLICATION_JSON);
		Response response = rb.status(Response.Status.OK).build();
		
		return response;
	}

}


