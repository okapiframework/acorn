/*===========================================================================
  Copyright (C) 2015 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.acorn.jsontapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@Path("dbpedia")
public class DBpedia {
	
	private class Candidate {
		
		public String url;
		public float lati; 
		public float longi;

		@Override
		public String toString () {
			return String.format("U: %s,  LT: %f, LG: %f\n", url, lati, longi);
		}
	};
	
	private final JSONParser parser = new JSONParser();

	private @Context ServletContext context;
	
	private String trgLang = "en";

	// For easier test
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response dbpedia_GET (@QueryParam("lat") String latitude,
		@QueryParam("long") String longitude,
		@QueryParam(value="lang") String lang)
	{
		System.out.println("=== GET request:");
		System.out.println("lat="+latitude+", long="+longitude+", lang="+lang);
		trgLang = lang.toLowerCase();
		return queryDBpedia(latitude, longitude, "{\"data\":\"content\"}");
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response dbpedia_POST (@QueryParam("lat") String latitude,
		@QueryParam("long") String longitude,
		@QueryParam("lang") String lang,
		InputStream inputStream)
	{
		System.out.println("=== POST request:");
		System.out.println("lat="+latitude+", long="+longitude+", input="+inputStream+", lang="+lang);
		trgLang = lang.toLowerCase();
		StringBuilder input = new StringBuilder();
		try ( BufferedReader br = new BufferedReader(new InputStreamReader(inputStream)) ) {
			String line = null;
			while (( line = br.readLine() ) != null ) {
				input.append(line);
			}
		}
		catch ( IOException e ) {
			e.printStackTrace();
			return ErrorResponse.create(Response.Status.BAD_REQUEST,
				"IO error:\n"+e.getMessage());
		}
		System.out.println("payload: "+input.toString());
		return queryDBpedia(latitude, longitude, input.toString());
	}

	private Response queryDBpedia (String latitude,
		String longitude,
		String jsonString)
	{
		if (( latitude == null ) || latitude.isEmpty() ) {
			return ErrorResponse.create(Response.Status.BAD_REQUEST,
				"Invalid latitude '"+latitude+"'");
		}
		if (( longitude == null ) || longitude.isEmpty() ) {
			return ErrorResponse.create(Response.Status.BAD_REQUEST,
				"Invalid longitude '"+longitude+"'");
		}
		if (( jsonString == null ) || jsonString.isEmpty() ) {
			return ErrorResponse.create(Response.Status.BAD_REQUEST,
				"Invalid JSON content '"+jsonString+"'");
		}
		
		String query = "http://live.dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&query=PREFIX+"
			+ "geo%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2003%2F01%2Fgeo%2Fwgs84_pos%23%3E%0D%0APREFIX+dbo%3A+%3Chttp"
			+ "%3A%2F%2Fdbpedia.org%2Fontology%2F%3E%0D%0ASELECT+*+WHERE+{%0D%0A%3Fs+a+dbo%3APlace+.%0D%0A%3Fs+a"
			+ "%3Chttp%3A%2F%2Fdbpedia.org%2Fclass%2Fyago%2FCity108524735%3E+.%0D%0A%3Fs+geo%3Alat+%3Flat+."
			+ "%0D%0A%3Fs+geo%3Along+%3Flong+.%0D%0AFILTER+%28+%0D%0A%3Flat+%3E+"
			+ latitude
			+ "+-+2+%26%26+%3Flat+%3C+"
			+ latitude
			+ "+%2B+2%0D%0A%26%26+%3Flong+%3E+"
			+ longitude
			+ "+-+2+%26%26+%3Flong+%3C+"
			+ longitude
			+ "+%2B+2%29%0D%0A}%0D%0ALIMIT+100&format=JSON&CXML_redir_for_subjs=121&CXML_redir_for_hrefs=&timeout="
			+ "0000&debug=on";
		
		URL url;
		StringBuffer dbp = new StringBuffer();
		try {
			url = new URL(query);
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
	 		int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
	 
			try ( BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream(), StandardCharsets.UTF_8)) )
			{
				String line;
				while (( line = in.readLine()) != null ) {
					dbp.append(line);
				}
			}
		}
		catch ( IOException e) {
			e.printStackTrace();
		}
		System.out.println(dbp.toString());
		
		String info = processResults(dbp.toString(), latitude, longitude);
	
		String res = "{\"all\":[" + jsonString + ","
			+ info + "]}";
		
		ResponseBuilder rb = Response.ok(res, MediaType.APPLICATION_JSON);
		Response response = rb.status(Response.Status.OK).build();
		return response;
	}

	private String processResults (String jsonText,
		String latitude,
		String longitude)
	{
		String res = "{\"info\": \"No point of interest nearby\"}";
		String resUrl = null;
		try {
			JSONObject o1 = (JSONObject)parser.parse(jsonText);
			JSONObject o2 = (JSONObject)o1.get("results");
			JSONArray a1 = (JSONArray)o2.get("bindings");
			if (( a1 != null ) && ( a1.size() > 0 )) {
				
				// Get the list of possible points of interest
				List<Candidate> candidates = new ArrayList<>();
				float baseLati = Float.parseFloat(latitude);
				if ( baseLati < 0 ) baseLati = baseLati*-1;
				float baseLongi = Float.parseFloat(longitude);
				if ( baseLongi < 0 ) baseLongi = baseLongi*-1;
				
				for ( int i=0; i<a1.size(); i++ ) {
					Candidate cand = new Candidate();
					JSONObject o3 = (JSONObject)a1.get(i);
					JSONObject o4 = (JSONObject)o3.get("s");
					if ( o4 != null ) {
						cand.url = (String)o4.get("value");
						o4 = (JSONObject)o3.get("lat");
						if ( o4 != null ) {
							cand.lati = diff(baseLati, (String)o4.get("value"));
							o4 = (JSONObject)o3.get("long");
							if ( o4 != null ) {
								cand.longi = diff(baseLongi, (String)o4.get("value"));
								candidates.add(cand);
							}
						}
					}
				}
				
				// Sort the candidates by latitude
				Collections.sort(candidates, new Comparator<Candidate>() {
					@Override
					public int compare (Candidate o1, Candidate o2) {
						return Float.compare(o1.lati, o2.lati);
					}
				});
				// Sort the candidates by longitude
				Collections.sort(candidates, new Comparator<Candidate>() {
					@Override
					public int compare (Candidate o1, Candidate o2) {
						return Float.compare(o1.longi, o2.longi);
					}
				});
				
				System.out.println(candidates);
				// Use the top candidate
				resUrl = candidates.get(0).url;
			}
			
			if ( resUrl == null ) {
				res = res + ",{\"url\":\"\"},{\"abstract\":\"\"}";
				return res;
			}
			res = "{\"info\":\"Point of interest\"}";
			res = res + ",{\"url\":\""+resUrl+"\"}";

			// Else: Try to get an abstract of the given resource
			URL url;
			StringBuffer tmp = new StringBuffer();
			try {
				url = new URL(resUrl);
				HttpURLConnection con = (HttpURLConnection)url.openConnection();
				con.setRequestMethod("GET");
				con.setRequestProperty("User-Agent", "Mozilla/5.0");
		 		int responseCode = con.getResponseCode();
				System.out.println("\nSending 'GET' request to URL : " + url);
				System.out.println("Response Code : " + responseCode);
				try ( BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getInputStream(), StandardCharsets.UTF_8)) )
				{
					String line;
					while (( line = in.readLine()) != null ) {
						tmp.append(line);
					}
				}
			}
			catch ( IOException e) {
				e.printStackTrace();
				res = res + ",{\"abstract\":\"An error occurred.\"}";
				return res;
			}
			
			// We get back an HTML page and use the top-level summary

			int p1 = tmp.indexOf("<div id=\"content\">");
			if ( p1 == -1 ) {
				res = res + ",{\"abstract\":\"\"}";
				return res;
			}
			int p2 = tmp.indexOf("<p>", p1);
			if ( p2 == -1 ) {
				res = res + ",{\"abstract\":\"\"}";
				return res;
			}
			int p3 = tmp.indexOf("</p>", p2);
			if ( p3 == -1 ) {
				res = res + ",{\"abstract\":\"\"}";
				return res;
			}
			String best = tmp.substring(p2+3, p3);
			best = best.trim();
			
			res = res + ",{\"abstract\":\""+DataStore.esc(unescapeHTML(best))+"\"}";
			
		}
		catch ( ParseException e ) {
			e.printStackTrace();
		}
		return res;
	}
	
	private float diff (float base,
		String valueStr)
	{
		float value = Float.parseFloat(valueStr);
		if ( value < 0 ) value = value*-1;
		return Math.abs(base-value);
	}

	private String unescapeHTML (String text) {
		text = text.replace("&quot;", "\"");
		text = text.replace("&apos;", "'");
		text = text.replace("&#39;", "'");
		text = text.replace("&lt;", "<");
		text = text.replace("&gt;", ">");
		text = text.replace("&amp;", "&");
		text = text.replace("&nbsp;", "\00a0");
		return text;
	}
}


