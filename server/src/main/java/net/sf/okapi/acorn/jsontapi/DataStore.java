/*===========================================================================
  Copyright (C) 2015 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.acorn.jsontapi;

import java.io.File;
import java.io.FileReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import net.sf.okapi.acorn.jsonaccess.JSONReaderWriter;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.query.QueryResult;
import net.sf.okapi.connectors.microsoft.MicrosoftMTConnector;
import net.sf.okapi.connectors.microsoft.Parameters;

import org.oasisopen.xliff.om.v1.IDocument;
import org.oasisopen.xliff.om.v1.IGroupOrUnit;
import org.oasisopen.xliff.om.v1.ISegment;
import org.oasisopen.xliff.om.v1.IUnit;

import com.mycorp.tmlib.Entry;
import com.mycorp.tmlib.SimpleTM;

public enum DataStore {

	/**
	 * The unique instance of the settings.
	 */
	SI;

	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
	
	final private JSONReaderWriter jrw = new JSONReaderWriter();

	private SimpleTM tm = null;
	private MicrosoftMTConnector mt = new MicrosoftMTConnector();
	
	private DataStore () {
		// Set up the MT connector
		try {
			// Get the credentials from a jsontapi.properties file
			// in the user's home directory
			Properties prop = new Properties();
			prop.load(new FileReader(new File(
				System.getProperty("user.home")+File.separator+"jsontapi.properties")));
			// Initializes the MT engine, and test it
			Parameters params = (Parameters) mt.getParameters();
			params.setClientId(prop.getProperty("clientId"));
			params.setSecret(prop.getProperty("secret"));
			mt.open(); // No clean way to close this, but it'll clean itself when being garbage collected
			mt.setLanguages(LocaleId.fromString("en"), LocaleId.fromString("fr"));
			mt.query("test");
		}
		catch ( Throwable e ) {
			e.printStackTrace();
			mt = null;
		}
		
		for ( int i=0; i<50; i++ ) System.out.println("");
		System.out.println("Okapi-Acorn Server for Translation API");
		System.out.println("MT engine: "+(mt==null ? "Not available" : mt.getSettingsDisplay()));
		System.out.println("");
	}
	
	public static String formatDate (Date date) {
		return dateFormat.format(date);
	}
	
	/**
	 * Escapes a string to JSON.
	 * A null parameter returns a null.
	 * @param text the text to escape.
	 * @return the escaped text.
	 */
	public static String esc (String text) {
		if ( text == null ) return null;
		return text.replaceAll("(\\\"|\\\\|/)", "\\$1");
	}
	
	/**
	 * Gets the JSON quoted string value or null of a string.
	 * @param text the text to quote
	 * @return the quoted and escaped text or null.
	 */
	public static String quote (String text) {
		if ( text == null ) return null;
		return "\""+text.replaceAll("(\\\"|\\\\|/)", "\\$1")+"\"";
	}

	public SimpleTM getTM () {
		return tm;
	}

	private SimpleTM loadTM (String trgLang) {
		// Load the TM data
		SimpleTM tm = new SimpleTM();
		int count = 0;
		URL url = getClass().getResource("data_"+trgLang+".tmx");
		if ( url != null ) {
			try {
				File inputFile = new File(url.toURI());
				count = tm.importSegments(inputFile, trgLang);
				System.out.println("TM entries imported = "+count+"");
			}
			catch ( URISyntaxException e ) {
				e.printStackTrace();
			}
		}
		else {
			System.out.println("TM resource 'data_"+trgLang+".tmx' not found.");
		}
		return tm;
	}
	
	public String processJSON (String jsonString,
		String srcLang,
		String trgLang)
	{
		// Read the translatable entries
		IDocument doc = jrw.load(jsonString);
		doc.setSourceLanguage(srcLang);
		doc.setTargetLanguage(trgLang);
		
		// Set the languages
		if ( mt != null ) {
			mt.setLanguages(LocaleId.fromString(doc.getSourceLanguage()),
				LocaleId.fromString(doc.getTargetLanguage()));
		}
		// Process them
		for ( IGroupOrUnit gou : doc.getFile("f1") ) {
			IUnit unit = (IUnit)gou;
			ISegment seg = unit.getSegment(0);
			
			if ( tm == null ) {
				tm = loadTM(trgLang);
			}
			
			// Try to find a match in the TM
			System.out.println("-- src:"+toVisible(seg.getSource().getCodedText()));
	    	Entry tmRes = tm.search(seg.getSource());
	    	if ( tmRes != null ) {
	    		System.out.println("TM match found");
	    		seg.copyTarget(tmRes.getTarget());
	    	}
	    	// Else: Fall back to MT if possible
	    	else if ( mt != null ) {
	    		mt.query(seg.getSource().getPlainText());
	    		if ( mt.hasNext() ) {
	    			QueryResult qr = mt.next();
    				System.out.println("MT score="+qr.getCombinedScore());
    				System.out.println("src:"+toVisible(qr.source.getText()));
    				System.out.println("trg:"+toVisible(qr.target.getText()));
    	    		seg.setTarget(qr.target.getText());
	    		}
	    	}
    		// We could improve the MT query by doing batch queries
		}
		
		// Reconstruct the JSON string and return it
		return jrw.mergeBack(doc);
	}

	public static String toVisible (String text) {
		if ( text == null ) return "null";
		// In XML 1.0 the valid characters are:
		// #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]
		StringBuilder tmp = new StringBuilder(text.length());
		for ( int i=0; i<text.length(); i++ ) {
			int cp = text.charAt(i);
			switch ( cp ) {
			case '&':
				tmp.append("&amp;");
				break;
			case '<':
				tmp.append("&lt;");
				break;
			case 0x0009:
			case 0x000A:
			case 0x000D:
				tmp.append((char)cp);
				continue;
			default:
				if ( cp > 0x007F ) {
					tmp.append(String.format("\\u%04X", cp));
				}
				else {
					tmp.append((char)cp);
				}
				continue;
			}
		}
		return tmp.toString();
	}
	
}
