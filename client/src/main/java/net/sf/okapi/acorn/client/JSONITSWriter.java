/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.acorn.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

import net.sf.okapi.acorn.common.IDocumentWriter;
import net.sf.okapi.acorn.jsonaccess.JSONReaderWriter;

import org.oasisopen.xliff.om.v1.IDocument;

public class JSONITSWriter implements IDocumentWriter {

	@Override
	public void merge (IDocument document,
		File originalFile)
	{
		try {
			JSONReaderWriter jrw = new JSONReaderWriter();
			jrw.load(originalFile);
		
			// We assume the original file path has an extension
			StringBuilder outPath = new StringBuilder(originalFile.getAbsolutePath());
			// We assume the original file path has an extension
			outPath.insert(outPath.lastIndexOf("."), "_"+document.getTargetLanguage());
			try ( OutputStreamWriter osw = new OutputStreamWriter(
					new FileOutputStream(new File(outPath.toString())), StandardCharsets.UTF_8) )
			{
				osw.write(jrw.mergeBack(document));
			}
		}
		catch ( Throwable e ) {
			throw new RuntimeException("Error in output. "+e.getMessage());
		}
	}

}
