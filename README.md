**Okapi Acorn** is project to experiment various localization-related technologies, for example:

   + A tentative / prototyping **object model for XLIFF**.  
   You can see the documentation for it here: **http://opentag.com/data/xliffomapi/**.  
   And the discussion list for this topic is here: https://lists.oasis-open.org/archives/xliff-users/.

   - A simple implementation of the **TAUS Translation API v2**.  
   See https://labs.taus.net/interoperability/taus-translation-api for more details.
   
   - An experimental implementation of **ITS in JSON** data, using JSONPath.  
   See https://www.w3.org/International/its/wiki/JSON+ITS