/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.acorn.jsonaccess;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

import org.junit.Test;
import org.oasisopen.xliff.om.v1.IDocument;
import org.oasisopen.xliff.om.v1.IFile;
import org.oasisopen.xliff.om.v1.IGroupOrUnit;
import org.oasisopen.xliff.om.v1.ISegment;
import org.oasisopen.xliff.om.v1.IUnit;

public class JSONReaderWriterTest {

	@Test
	public void testSimpleTargetChange () {
		JSONReaderWriter jrw = new JSONReaderWriter();
		String original = "{"+createData1()+"}";
		IDocument doc = jrw.load(original);
		
		for ( IGroupOrUnit gou : doc.getFile("f1") ) {
			IUnit unit = (IUnit)gou;
			ISegment seg = unit.getSegment(0);
			seg.copyTarget(seg.getSource());
			seg.getTarget().setCodedText(seg.getTarget().getCodedText()+"___zzz]");
		}
		String res = jrw.mergeBack(doc);
		String expected = "{\"messages\":["
			+ "{\"text\":\"Text 1___zzz]\",\"other\":\"data1___zzz]\"},"
			+ "{\"text\":\"Text 2___zzz]\",\"other\":\"data2___zzz]\"}"
			+ "],"
			+ "\"extra\":\"extra-data___zzz]\"}";
		assertEquals(expected, res);
	}

	@Test
	public void testReadFromFile ()
		throws URISyntaxException
	{
		JSONReaderWriter jrw = new JSONReaderWriter();
		URL url = getClass().getResource("input.json");
		File inputFile = new File(url.toURI());
		IDocument doc = jrw.load(inputFile);
		IFile file = doc.getFile("f1");
		int count = 0;
		for ( IGroupOrUnit gou : file ) {
			if ( gou instanceof IUnit ) {
				count++;
				IUnit unit = (IUnit)gou;
				System.out.println("s"+count+"="+unit.getSegment(0).getSource().getPlainText());
			}
		}
		assertEquals(1, count);
	}
	
	@Test
	public void testEncoding () {
		JSONReaderWriter jrw = new JSONReaderWriter();
		IDocument doc = jrw.load("{\"text\":\"\u2103\u00C8\u062A etc.\"}");
		IFile file = doc.getFile("f1");
		int count = 0;
		for ( IGroupOrUnit gou : file ) {
			if ( gou instanceof IUnit ) {
				count++;
				IUnit unit = (IUnit)gou;
				assertEquals("\u2103\u00C8\u062A etc.", unit.getSegment(0).getSource().getCodedText());
			}
		}
		assertEquals(1, count);
	}
	
	private String createData1 () {
		return "\"messages\":["
			+ "{\"text\":\"Text 1\",\"other\":\"data1\"},"
			+ "{\"text\":\"Text 2\",\"other\":\"data2\"}"
			+ "],"
			+ "\"extra\":\"extra-data\"";
	}

}
