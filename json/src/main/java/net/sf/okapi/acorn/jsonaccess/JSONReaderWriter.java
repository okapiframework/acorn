/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.acorn.jsonaccess;

import java.io.File;
import java.io.FileInputStream;

import net.sf.okapi.acorn.common.IDocumentReader;
import net.sf.okapi.acorn.xom.Factory;
import net.sf.okapi.common.LocaleId;

import org.oasisopen.xliff.om.v1.IDocument;
import org.oasisopen.xliff.om.v1.IFile;
import org.oasisopen.xliff.om.v1.IGroupOrUnit;
import org.oasisopen.xliff.om.v1.ISegment;
import org.oasisopen.xliff.om.v1.IUnit;
import org.oasisopen.xliff.om.v1.IXLIFFFactory;

public class JSONReaderWriter implements IDocumentReader {

	private static final IXLIFFFactory xf = Factory.XOM;

	private JSONAccess ja;
	private LocaleId srcLoc = LocaleId.ENGLISH;
	private LocaleId trgLoc = LocaleId.FRENCH;

	@Override
	public IDocument load (File inputFile) {
		ja = new JSONAccess();
		// For now we need to open 2 copies of the same file to read
		// because we need the context twice
		try ( FileInputStream fis1 = new FileInputStream(inputFile) ) {
			try ( FileInputStream fis2 = new FileInputStream(inputFile) ) {
				ja.read(fis1, fis2);
				return read();
			}
		}
		catch (Throwable  e) {
			throw new RuntimeException("Error reading document.", e);
		}
	}

	@Override
	public IDocument load (String inputString) {
		ja = new JSONAccess();
		ja.read(inputString);
		return read();
	}

	private IDocument read () {
		IDocument doc = null;
		doc = xf.createDocument();
		doc.setSourceLanguage(srcLoc.toString());
		doc.setTargetLanguage(trgLoc.toString());
		IFile file = doc.add(xf.createFile("f1"));
		while ( ja.hasNext() ) {
			String text = ja.next();
			String id = toNmtoken(ja.getEntryPath());
			IUnit unit = xf.createUnit(id);
			ISegment seg = unit.appendSegment();
			seg.setSource(text);
			seg.setId("s1");
			unit.setName(ja.getEntryPath());
			file.add(unit);
		}
		return doc;
	}

	private String toNmtoken (String text) {
		StringBuilder tmp = new StringBuilder();
		for ( int i=0; i<text.length(); i++ ) {
			char ch = text.charAt(i);
			switch ( ch ) {
			case '$':
			case '[':
			case ']':
				tmp.append('_');
				break;
			case '\'':
				break;
			default:
				tmp.append(ch);
				break;
			}
		}
		return tmp.toString();
	}
	
	public String mergeBack (IDocument doc) {
		ja.applyRules();
		
		// File id should be f1 (see above)
		IFile file = doc.getFile("f1");
		for ( IGroupOrUnit gou : file ) {
			IUnit unit = (IUnit)gou;
			// Get the same entry in JSON
			if ( !ja.hasNext() ) {
				throw new RuntimeException("JSON document not synchronized with XLIFF document.");
			}
			ja.next();
			String path = ja.getEntryPath();
			String pathId = toNmtoken(path);
			if ( !pathId.equals(unit.getId()) ) {
				throw new RuntimeException(String.format("Mismatching entry, JSON=%s, XLIFF=%s.",
					path, unit.getId()));
			}
			ISegment seg = unit.getSegment(0); //TODO: maybe we should join-all before?
			if ( seg.hasTarget() && !seg.getTarget().isEmpty() ) {
				ja.setNewValue(seg.getTarget().getCodedText()); // For now plain text only
			}
		}
		return ja.getOutput();
	}
}
