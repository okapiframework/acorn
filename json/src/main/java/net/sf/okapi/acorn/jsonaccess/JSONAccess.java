/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.acorn.jsonaccess;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.minidev.json.JSONArray;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.PathNotFoundException;
import com.jayway.jsonpath.spi.json.JsonProvider;

/**
 * Provides access to translatable strings for a JSON string.
 * The rules to specify which strings are translatable are provided through a special JSON string
 * which can be included in the data to read.
 * Rules are applied in the order they are declared and the last one overrides any previous one
 * when they resolve on the same node.
 * <p>Each rule must be in the format:
 * <pre>
 * {
 *  "selector":"&lt;json-path>",
 *  "translate":true|false (optional, default is true)
 * }
 * </pre>
 */
public class JSONAccess {

	final static String ITSRULE_PATH = "$..itsRules";
	
	final private JsonProvider jp;
	final private Configuration confP;
	final private List<Rule> firstDefaultRules;
	final private List<Rule> lastDefaultRules;

	private DocumentContext dcV;
	private DocumentContext dcP;
	private List<Rule> rules;
	private LinkedHashMap<String, Node> nodes;
	private Iterator<Entry<String, Node>> nodesIter;
	private Entry<String, Node> currentEntry;

	private class Node {
		
		boolean trans;
		String text;
		
		@Override
		public String toString () {
			return "tx:"+text+" tr:"+trans;
		}
	
	}
	
	/**
	 * Creates a new JSONAccess object.
	 */
	public JSONAccess () {
		jp = Configuration.defaultConfiguration().jsonProvider();
		confP = Configuration.builder().options(Option.AS_PATH_LIST).options(Option.SUPPRESS_EXCEPTIONS).build();
		// Default rules:
		// Translate all nodes, except for itsRules
		firstDefaultRules = new ArrayList<>();
		firstDefaultRules.add(new Rule("$..*", true));
		lastDefaultRules = new ArrayList<>();
		lastDefaultRules.add(new Rule(ITSRULE_PATH, false));
	}

	/**
	 * Gets the current rules.
	 * @return the current rules (can be null or empty).
	 */
	public List<Rule> getRules () {
		return rules;
	}
	
	/**
	 * Sets new rules.
	 * @param rulesString the rules string (can be null).
	 * @return the JSONAccess object itself (to allowed dot-operations).
	 */
	public JSONAccess setRules (String rulesString) {
		resetCursor();
		if ( rulesString == null ) {
			rules = null;
		}
		else {
			List<Object> list = JsonPath.read(jp.parse(rulesString), ITSRULE_PATH);
			compileRules(list);
		}
		return this;
	}

	/**
	 * Compiles a set of new rules.
	 * @param list the list of rules as JSON objects.
	 * (if null or empty: it results on undefined rules) 
	 */
	private void compileRules (List<Object> list) {
		if (( list == null ) || list.isEmpty() ) {
			rules = null;
			return;
		}
		// Else: process each rule
		rules = new ArrayList<>();
		try {
			for ( Object obj : list ) {
				if ( obj instanceof JSONArray ) {
					JSONArray array = (JSONArray)obj;
					for ( int i=0; i<array.size(); i++ ) {
						@SuppressWarnings("unchecked")
						Map<String, Object> map = (Map<String, Object>)array.get(i);
						String selector = (String)map.get("selector");
						boolean translate = true; // default
						if ( map.containsKey("translate") ) {
							translate = (boolean)map.get("translate");
						}
						rules.add(new Rule(selector, translate));
					}
				}
				else if ( obj instanceof Map ) { 
					@SuppressWarnings("unchecked")
					Map<String, Object> map = (Map<String, Object>)obj;
					String selector = (String)map.get("selector");
					boolean translate = true; // default
					if ( map.containsKey("translate") ) {
						translate = (boolean)map.get("translate");
					}
					rules.add(new Rule(selector, translate));
				}
			}
		}
		catch ( Throwable e ) {
			throw new RuntimeException("Syntax error in rule: "+e.getMessage());
		}
	}
	
	/**
	 * Sets a string as the input to process. The input may include the rules to use.
	 * If the input includes the rules, they become the new rules. 
	 * @param jsonString the input to set.
	 * @return the JSONAccess object itself (to allowed dot-operations).
	 */
	public JSONAccess setInput (String jsonString) {
		resetCursor();
		dcV = JsonPath.parse(jsonString);
		dcP = JsonPath.using(confP).parse(jsonString);
		// Check if the rules are present in the data
		try {
			List<Object> tmp = dcV.read(ITSRULE_PATH);
			compileRules(tmp);
		}
		catch ( PathNotFoundException e ) {
			// Do nothing: this is not an error.
		}
		return this;
	}
	
	/**
	 * Sets an input stream as the input to process. The input may include the rules to use.
	 * If the input includes the rules, they become the new rules. 
	 * @param input1 the input to set.
	 * @param input2 the input to set (same as input1, but a different stream).
	 * @return the JSONAccess object itself (to allowed dot-operations).
	 */
	public JSONAccess setInput (InputStream input1,
		InputStream input2)
	{
		resetCursor();
		dcV = JsonPath.parse(input1);
		dcP = JsonPath.using(confP).parse(input2);
		// Check if the rules are present in the data
		try {
			List<Object> tmp = dcV.read(ITSRULE_PATH);
			compileRules(tmp);
		}
		catch ( PathNotFoundException e ) {
			// Do nothing: this is not an error.
		}
		return this;
	}
	
	/**
	 * Reads a given input string which may include new rules, and apply the latest rules.
	 * If the input includes the rules, they become the current rules and are applied.
	 * Otherwise some rules are already set they remain the current rules and are applied.
	 * If no rules are set, no rules are applied yet.
	 * @param inputString the data string to read.
	 */
	public void read (String inputString) {
		setInput(inputString);
		applyRules();
	}

	/**
	 * Reads a given input stream which may include new rules, and apply the latest rules.
	 * If the input includes the rules, they become the current rules and are applied.
	 * Otherwise some rules are already set they remain the current rules and are applied.
	 * If no rules are set, no rules are applied yet.
	 * @param inputString the data string to read.
	 */
	public void read (InputStream input1,
		InputStream input2)
	{
		setInput(input1, input2);
		applyRules();
	}

	/**
	 * Reads a given input string with the rules provided as a separate string, and applies the rules.
	 * If the input string has rules they are not used, instead the rules specified
	 * separately are used.
	 * @param inputString the string to read.
	 * @param rulesString the string with the rules.
	 */
	public void read (String inputString,
		String rulesString)
	{
		setInput(inputString);
		applyRules(rulesString);
	}
	
	/**
	 * Applies new rules to the last data set.
	 * @param rulesString the new rules.
	 */
	public void applyRules (String rulesString) {
		setRules(rulesString);
		applyRules();
	}

	/**
	 * Applies the current rules to the current input.
	 * Both rules and input must not be null.
	 */
	public void applyRules () {
		resetCursor();
		if ( dcV == null ) {
			throw new RuntimeException("No input data are set.");
		}
		if ( rules == null ) {
			rules = new ArrayList<>();
			//System.out.println("Applies default JSON+ITS rule.");
		}

		// Apply the rule
		nodes = new LinkedHashMap<>();
		// The defaults to process first
		processRules(firstDefaultRules);
		// The user-defined rules
		processRules(rules);
		// The default to process last
		processRules(lastDefaultRules);
	}
	
	private void processRules (List<Rule> rules) {
		// First: Create nodes for all rules with flag for properties
		for ( Rule rule : rules) {
			List<String> resP = dcP.read(rule.getSelector());
			if (( resP == null ) || resP.isEmpty() ) continue;
			// Else: we have at least one match
			for ( String path : resP ) {
				// Try to get existing node (created from previous rule)
				Node node = nodes.get(path);
				// If it does not exists: create it and add it
				if ( node == null ) {
					node = new Node();
					nodes.put(path, node);
				}
				// Then set the latest properties
				node.trans = rule.getTranslate();
			}
			// Check any nodes starting with that path (overrides)
			String rulePath = resP.get(0);
			for ( String path : nodes.keySet() ) {
				if ( path.startsWith(rulePath) ) {
					nodes.get(path).trans = rule.getTranslate();
				}
			}
		}

		// Next: Remove all nodes that are not translatable
		// or get the text for the nodes that are translatable
		Iterator<Entry<String, Node>> iter = nodes.entrySet().iterator();
		while ( iter.hasNext() ) {
			Entry<String, Node> entry = iter.next();
			if ( entry.getValue().trans ) {
				// Get the text
				Object obj = dcV.read(entry.getKey());
				if ( obj instanceof String ) {
					entry.getValue().text = (String)obj;
				}
				else {
					iter.remove();
				}
			}
			else { // Not translatable: remove it
				iter.remove();
			}
		}
		
		if ( !nodes.isEmpty() ) {
			nodesIter = nodes.entrySet().iterator();
		}
		else {
			nodes = null;
		}
	}

	/**
	 * Indicates if the last JSON entry read has another translatable string.
	 * @return true if there is another translatable string, false otherwise.
	 */
	public boolean hasNext () {
		if ( nodesIter == null ) return false;
		return nodesIter.hasNext();
	}
	
	/**
	 * Gets the next translatable string of the last JSON string read.
	 * You must call {@link #hasNext()} before.
	 * @return the next translatable string.
	 */
	public String next () {
		currentEntry = nodesIter.next();
		return currentEntry.getValue().text;
	}

	public String getEntryPath () {
		return currentEntry.getKey();
	}

	/**
	 * Sets a new text value (the translation) for the current translatable string.
	 * @param newValue the new text value to set.
	 */
	public void setNewValue (String newValue) {
		dcV.set(currentEntry.getKey(), newValue);
	}

	/**
	 * Gets the JSON string after modifications done using {@link #setNewValue(String)}.
	 * @return the JSON string of the input with any modifications, or null
	 * if there is no input specified.
	 */
	public String getOutput () {
		if ( dcV == null ) return null;
		return dcV.configuration().jsonProvider().toJson(dcV.json());
	}

	/**
	 * Resets the variables used for iterating through the translatable strings.
	 */
	private void resetCursor () {
		nodes = null;
		nodesIter = null;
	}

}
